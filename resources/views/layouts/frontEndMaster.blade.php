<!DOCTYPE html>
<html lang="en">
  <head>
    <title>PROTAFOLIO</title>
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('font-awesome/css/font-awesome.css') !!}
    {!! Html::style('css/blog.css') !!}
    {!! Html::script('js/jquery-2.1.1.js') !!}
    
   
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    

  </head>
  <body>
   @yield('header')
    <!--<div class="container">
      <!--
      <div class="blog-header">
        <h4 class="blog-title">Andres Londoño</h4>
        <p class="lead blog-description">Tecnologo Analisis y Desarrollo de Sistemas de la Información (ADSI)</p>
        <p class="lead blog-description">Joven empresario de la industria libre con manejo en creacion de Sistemas de Informaciòn utilizando Laravel y Mysql principalmente aunque acepta trabajos en otros programas</p>
        <p class="lead blog-description">Telefono: 3207269584</p>
        <p class="lead blog-description">Correo: aclodono87@misena.edu.co</p>
      </div>-->
      <div class="row">
            @yield('content')
            @yield('sidebar')
      </div>
    </div>
    @yield('footer')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    {!! Html::script('js/bootstrap.min.js') !!}
  </body>
</html>
