<h3>Buscar Resultado</h3>
<table class="table table-responsive table-bordered  table-hover">
    <thead>
    <tr>
        <th class="text-center">Codigo del trabajo</th>
        <th class="text-center">Titulo del trabajo</th>
        <th class="text-center">Fecha limite</th>
        <th class="text-center">Departamento</th>
        <th class="text-center">Aplicaciones</th>
        <th class="text-center">Actiones</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($jobPosts as $jobPost)
            <tr>
                <td class="text-center">{{ $jobPost->autoGeneratedJobId }}</td>
                <td class="text-center">{{ $jobPost->jobTitle }}</td>
                <td class="text-center">{{ $jobPost->deadline }}</td>
                <td class="text-center">{{ $jobPost->department->departmentName }}</td>
                <td class="text-center">
                    @if(count($jobPost->application))
                        <a href="{{ URL::to('admin/job/application-to-job/'.$jobPost->autoGeneratedJobId) }}" class="btn btn-sm btn-success">
                        {{ count($jobPost->application) }}
                        </a>
                    @else
                        No Application
                    @endif

                </td>
                <td class="text-center">
                    <a href="{{ URL::to('admin/job/edit/'.$jobPost->id) }}" class="btn btn-sm btn-info">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ URL::to('admin/job/show/'.$jobPost->id) }}" class="btn btn-sm btn-success">
                        <i class="fa fa-eye"></i>
                    </a>
                    <a href="{{ URL::to('admin/job/delete/'.$jobPost->id) }}" class="btn btn-sm btn-danger" onClick="return checkDelete();">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>