<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Portafolio</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/ui/trumbowyg.css') }}">

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">PORTAFOLIO</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Perfil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Servicios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead">
      <div class="header-content">
        <div class="header-content-inner">
          <h1 id="homeHeading">VICTOR MANUEL GUEVARA.</h1>
          <hr>
          <p> SOY UNA PERSONA EMPRENDEDORA CAPAZ DE ASUMIR RETOS, TAMBIEN SOY UNA PERSONA AMANTE DEL DEPORTE Y EL AMOR PORLOS MOTORES. </p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">VER MAS...</a>
        </div>
      </div>
    </header>

      
    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">PERFIL</h2>
            <hr class="light">
            
          @foreach($jobPosts as $jobPost)
            <div class="blog-post">
             
              <h3>{{ $jobPost->jobTitle }}</h3>
              <pre><code>Fecha : {{ $jobPost->deadline }} | Departamento : {{ $jobPost->department->departmentName }}</code></pre>
              <p>{{ $jobPost->jobDescription }}</p>
            </div>
          @endforeach
            <a class="btn btn-default btn-xl js-scroll-trigger" href="#services">VER MAS...</a>
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">SERVICIOS</h2>
            <hr class="primary">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-diamond text-primary sr-icons"></i>
              <h3>Diseño Wed</h3>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
              <h3>Análisis de informacion</h3>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
              <h3>Creacion de bases de datos</h3>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box">
              <i class="fa fa-4x fa-heart text-primary sr-icons"></i>
              <h3>Desarrollo wed</h3>
              <p class="text-muted"></p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="p-0" id="portfolio">
      <div class="container-fluid">
        <div class="row no-gutter popup-gallery">
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/sql.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/sql.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                   
                  </div>
                  <div class="project-name">
                     SQL SERVER
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/mysql.png">
              <img class="img-fluid" src="img/portfolio/thumbnails/mysql.png" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    
                  </div>
                  <div class="project-name">
                    MY SQL
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/php.png">
              <img class="img-fluid" src="img/portfolio/thumbnails/php.png" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                   
                  </div>
                  <div class="project-name">
                   PHP
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/java.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/java.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    
                  </div>
                  <div class="project-name">
                    JAVA
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/c.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/c.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    
                  </div>
                  <div class="project-name">
                    C#
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/laravel.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/laravel.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                   
                  </div>
                  <div class="project-name">
                   LARAVEL
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

   
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">CONTACTO</h2>
            <hr class="primary">
            
         

 
            {!! Form::open(array('url'=>'/apply-to-job/'.$jobPost->autoGeneratedJobId, 'method'=>'POST', 'class'=>'form', 'enctype'=>'multipart/form-data', 'role'=>'form')) !!}
            
                <div class="form-group <?php  if($errors->has('fullName')){ echo 'has-error'; }?> ">
                    <label for="fullName" class="">
                        Nombre Completo
                    </label>
                     <input type="text" name="fullName" id="fullName" value="{{ Input::old('fullName') }}" class="form-control">
                    @if ($errors->has('fullName')) <p class="help-block m-b-none">{{ $errors->first('fullName') }}</p> @endif
                </div>
                <input type="hidden" name="jobId" id="jobId" value="{{ $jobPost->autoGeneratedJobId }}" />
                <input type="hidden" name="departmentId" id="departmentId" value="{{ $jobPost->departmentId }}" />
                <div class="form-group <?php  if($errors->has('mobileNumber')){ echo 'has-error'; }?> ">
                    <label for="mobileNumber" class="">
                       Celular Del Cliente
                    </label>
                    <input type="text" name="mobileNumber" id="mobileNumber" value="{{ Input::old('mobileNumber') }}"  class="form-control">
                    @if ($errors->has('mobileNumber')) <p class="help-block m-b-none">{{ $errors->first('mobileNumber') }}</p> @endif
                </div>
               
           


           
               <div class="form-group <?php  if($errors->has('email')){ echo 'has-error'; }?> ">
                   <label for="email" class="">
                       Email
                   </label>
                   <input type="text" name="email" id="email" value="{{ Input::old('email') }}"  class="form-control">
                   @if ($errors->has('email')) <p class="help-block m-b-none">{{ $errors->first('email') }}</p> @endif
               </div>
                <div class="form-group <?php  if($errors->has('expectedSalary')){ echo 'has-error'; }?> ">
                    <label for="expectedSalary" class="">
                        Pago Por El Aplicativo
                    </label>
                    <input type="text" name="expectedSalary" id="expectedSalary" value="{{ Input::old('expectedSalary') }}"  class="form-control">
                    @if ($errors->has('expectedSalary')) <p class="help-block m-b-none">{{ $errors->first('expectedSalary') }}</p> @endif
                </div>
                <div class="form-group <?php  if($errors->has('address')){ echo 'has-error'; }?> ">
                    <label for="address" class="">
                     Descripcion
                    </label>
                    <textarea type="text" name="address" id="address" value="{{ Input::old('address') }}"  class="form-control txtArea"></textarea>
                    @if ($errors->has('address')) <p class="help-block m-b-none">{{ $errors->first('address') }}</p> @endif
                </div>
                 <div class="form-group <?php  if($errors->has('picture')){ echo 'has-error'; }?> ">
                    <label for="picture" class="">
                        Foto Del Cliente
                    </label>
                    <input type="file" name="picture" id="picture" value="{{ Input::old('picture') }}" class="form-control">
                    @if ($errors->has('picture')) <p class="help-block m-b-none">{{ $errors->first('picture') }}</p> @endif
                </div>
                <div class="form-group <?php  if($errors->has('cv')){ echo 'has-error'; }?> ">
                    <label for="cv" class="">
                        PDF con la descripcion del aplicativo
                    </label>
                    <input type="file" name="cv" id="cv" value="{{ Input::old('cv') }}" class="form-control">
                    @if ($errors->has('cv')) <p class="help-block m-b-none">{{ $errors->first('cv') }}</p> @endif
                </div>
                <div class="form-group">
                    <label for="" class=""> </label>
                   <center> <button class="btn btn-md btn-primary" style="margin-top: 10px;" type="submit">Enviar Peticion</button></center>
                </div>

            
            {!! Form::close() !!}
        </div>
      </div>
    </div>
 </div>
        </div>

        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-phone fa-3x sr-contact"></i>
            <p>319-274-3329</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fa fa-envelope-o fa-3x sr-contact"></i>
            <p>
              <a href="mailto:your-email@your-domain.com">mvguevara@misena.edu.co</a>
            </p>
          </div>
        </div>
      </div>
    </section>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ asset('dist/trumbowyg.js') }}"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>
   
    <script>
        $('.txtArea').trumbowyg();
    </script>

  </body>

</html>