@extends('layouts.frontEndMaster')
@section('content')


<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


    <div class="col-lg-9 blog-main">
      @foreach($jobPosts as $jobPost)
          <div class="blog-post">
            <h3>{{ $jobPost->jobTitle }}</h3>
            <pre><code>Fecha : {{ $jobPost->deadline }} | Departamento : {{ $jobPost->department->departmentName }}</code></pre>
            <p>{{ $jobPost->jobDescription }}</p>
            <nav>
                <ul class="pager">
                  <li><a href="{{ URL::to('/apply-to-job/'.$jobPost->autoGeneratedJobId) }}">Solicitar Aplicacion</a></li>
                  <li><a href="#">Lee Mas</a></li>
                </ul>
             </nav>
          </div>
      @endforeach
    </div>
@endsection